TARGET=devmem

CC = gcc
CFLAGS = -g -Wall
LDFLAGS =
LIBS =

all: $(TARGET)

$(TARGET): main.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) *.o $(TARGET)

.PHONY: all clean
