#define DEBUG_VERBOSE 1

#include <stdlib.h>   // strtoull EXIT_SUCCESS EXIT_FAILRE
#include <stdio.h>    // fprintf perror stdout stderr
#include <stdint.h>   // uint8_t uint16_t uint32_t uint64_t
#include <errno.h>    // errno
#include <string.h>   // strlen strcmp
#include <ctype.h>    // isdigit
#include <fcntl.h>    // open O_RDWR O_RDONLY O_SYNC
#include <unistd.h>   // close getpagesize
#include <sys/mman.h> // mmap munmap
#include <libgen.h>   // basename

#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

#define OFF_PRI "l"
#define OFF_FMT "%"OFF_PRI"u"

static const char *program_name;

int show_usage()
{
    printf("usage: %s ADDRESS [WIDTH [VALUE]]\n", program_name);
    printf("\nRead/write from physical address\n\n");
    printf("  ADDRESS    Address to act upon\n");
    printf("  WIDTH      Width (8/16/32/64 or b/h/w/l)\n");
    printf("  VALUE      Data to be written\n");
    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    program_name = basename(argv[0]);
    uint64_t writeval = writeval; /* for compiler */
    unsigned width = 8 * sizeof(int);

    /* ADDRESS */
    if (!argv[1] || strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
        return show_usage();

    errno = 0;
    char *endptr;
    off_t target = strtoull(argv[1], &endptr, 0); /* allow hex, oct, etc */
    if (errno || *endptr != '\0') {
        fprintf(stderr, "Unable to parse ADDRESS string\n");
        return EXIT_FAILURE;
    }

    /* WIDTH */
    if (argv[2]) {
        if (isdigit(argv[2][0]) || argv[2][1]) {
            errno = 0;
            width = strtoull(argv[2], &endptr, 10);
            if (errno || *endptr != '\0') {
                fprintf(stderr, "Unable to parse WIDTH string\n");
                return EXIT_FAILURE;
            }
        }
        else {
            static const char bhwl[] = "bhwl";
            static const uint8_t sizes[] = {
                8 * sizeof(char),
                8 * sizeof(short),
                8 * sizeof(int),
                8 * sizeof(long)
            };
            width = 0; /* bad */
            char c = argv[2][0] | 0x20;
            for (int i = 0; i < strlen(bhwl); ++i) {
                if (c == bhwl[i]) {
                    width = sizes[i];
                    break;
                }
            }
            if (width == 0) {
                fprintf(stderr, "Unable to parse WIDTH string\n");
                return EXIT_FAILURE;
            }
        }
        /* VALUE */
        if (argv[3]) {
            errno = 0;
            writeval = strtoull(argv[3], &endptr, 0); /* allow hex, oct, etc */
            if (errno || *endptr != '\0') {
                fprintf(stderr, "Unable to parse VALUE string\n");
                return EXIT_FAILURE;
            }
        }
    } else { /* argv[2] == NULL */
        /* make argv[3] to be a valid thing to fetch */
        argv--;
    }

    int fd = open("/dev/mem", argv[3] ? (O_RDWR | O_SYNC) : (O_RDONLY | O_SYNC));
    if (fd < 0) {
        perror("open(/dev/mem)");
        return EXIT_FAILURE;
    }

    unsigned page_size;
    unsigned mapped_size = page_size = getpagesize();
    unsigned offset_in_page = (unsigned)target & (page_size - 1);
    if (offset_in_page + width > page_size) {
        /* This access spans pages.
         * Must map two pages to make it possible: */
        mapped_size *= 2;
    }

    void *map_base = mmap(NULL,
                    mapped_size,
                    argv[3] ? (PROT_READ | PROT_WRITE) : PROT_READ,
                    MAP_SHARED,
                    fd,
                    target & ~(off_t)(page_size - 1));
    if (map_base == MAP_FAILED) {
        perror("mmap");
        return EXIT_FAILURE;
    }

#if defined(DEBUG_VERBOSE) && DEBUG_VERBOSE >= 1
    fprintf(stdout, "Memory mapped at address %p.\n", map_base);
#endif

    void *virt_addr = (char *)map_base + offset_in_page;

    if (!argv[3]) {
        uint64_t read_result;
		switch (width) {
		case 8:
			read_result = *(volatile uint8_t*)virt_addr;
			break;
		case 16:
			read_result = *(volatile uint16_t*)virt_addr;
			break;
		case 32:
			read_result = *(volatile uint32_t*)virt_addr;
			break;
		case 64:
			read_result = *(volatile uint64_t*)virt_addr;
			break;
		default:
			fprintf(stderr, "bad width, expected 8/16/32/64\n");
            return EXIT_FAILURE;
		}
#if defined(DEBUG_VERBOSE) && DEBUG_VERBOSE >= 1
		fprintf(stdout, "Value at address 0x%"OFF_PRI"X (%p): 0x%llX\n",
			target, virt_addr,
			(unsigned long long)read_result);
#endif
		/* Zero-padded output shows the width of access just done */
		fprintf(stdout, "0x%0*llX\n", (width >> 2), (unsigned long long)read_result);
	} else {
        uint64_t read_result;
		switch (width) {
		case 8:
			*(volatile uint8_t*)virt_addr = writeval;
			read_result = *(volatile uint8_t*)virt_addr;
			break;
		case 16:
			*(volatile uint16_t*)virt_addr = writeval;
			read_result = *(volatile uint16_t*)virt_addr;
			break;
		case 32:
			*(volatile uint32_t*)virt_addr = writeval;
			read_result = *(volatile uint32_t*)virt_addr;
			break;
		case 64:
			*(volatile uint64_t*)virt_addr = writeval;
			read_result = *(volatile uint64_t*)virt_addr;
			break;
		default:
            perror("bad width");
            return EXIT_FAILURE;
		}
#if defined(DEBUG_VERBOSE) && DEBUG_VERBOSE >= 1
		fprintf(stdout, "Written 0x%llX; readback 0x%llX\n",
			(unsigned long long)writeval,
			(unsigned long long)read_result);
#else
        (void)read_result; // unused - for compiler
#endif
	}

	if (munmap(map_base, mapped_size) == -1)
		perror("munmap");
	close(fd);
    return EXIT_SUCCESS;
}